# Android SDK image for Docker #

### Supported Platforms ###

- Android API levels

    - API 24 - Nougat - 7.0
    - API 23 - Marshmallow - 6.0
    - API 22 - Lollipop - 5.1
    - API 21 - Lollipop - 5.0
    - API 20 - KitKat - 4.4
    - API 19 - KitKat - 4.4
    - API 18 - Jelly Bean - 4.3
    - API 17 - Jelly Bean - 4.2

- Builds Tools
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Building ###

This dockerfile is set to automatically build on hub.docker.com. However, if you want to build it yourself, you can do so by running:

```
docker build android-sdk
```

You might need to increase the default 10G base disk size; you can do this by editing `/etc/sysconfig/docker-storage` and adding the following
configuration:

```
DOCKER_STORAGE_OPTIONS="--storage-opt dm.basesize=32G"
```

